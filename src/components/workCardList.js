import * as React from "react";
import { WorkCard } from './workCard';

export class WorkCardList extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    let listOfWork = this.props.workList.map((work, index) => {
      work.id = index+1;
      return (
        <WorkCard key={index} work={work} editWork={this.props.editWork} deleteWork={this.props.deleteWork} />
      )
    });

    return (
      <div className="row h-100 my-3 section">
        {listOfWork}
      </div>
    )
  }
}
