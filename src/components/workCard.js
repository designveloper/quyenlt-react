import * as React from 'react';
import { AddNewModal } from './../react/pages/addNewModal';

export class WorkCard extends React.Component {

  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this);
    this.delete = this.delete.bind(this);
  }

  renderStatus(status) {
    if (status == 0) {
      return (
        "To do"
      )
    } else if (status == 1) {
      return (
        "Doing"
      )
    } else if (status == 2) {
      return (
        "Done"
      )
    } else {
      return (
        "Pending"
      )
    }
  }

  submit(e) {
    e.preventDefault();
    let today = new Date();
    var work = {
      title: this.refs.title.value,
      description: this.refs.description.value,
      status: this.refs.status.value,
      dateEnd: this.refs.dateEnd.value,
      dateCreated: today.getFullYear()+"-"+today.getMonth()+"-"+today.getDate()
    }

    var position = Number(this.props.work.id) - 1;
    this.props.editWork(position, work);

    $(`#${this.props.work.id}`).modal('hide');
  }

  delete(e) {
    e.preventDefault();
    var position = Number(this.props.work.id) - 1;
    $(`#delete${this.props.work.id}`).modal('hide');
    this.props.deleteWork(position);
  }

  render() {

    let id = "#" + this.props.work.id;
    let idDelete = "#delete" + this.props.work.id;
    let status = Number(this.props.work.status);

    return (
      <div className="col-xl-4 col-sm-6 mb-3">
        <div className="card">
          <div className="card-header">
            #{this.props.work.id}

            <button type="button" className="close" data-toggle="modal" data-target={idDelete}>
              <span aria-hidden="true">&times;</span>
            </button>
            <div className="modal fade" id={`delete${this.props.work.id}`} tabIndex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="false">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">DELETE</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={this.delete}>
                      <h3>Are you sure?</h3>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" className="btn btn-primary">Yes</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="card-body">
            <h4> {this.props.work.title} </h4>
            <p>{this.props.work.description}</p>
            {/* <span> Date Create: {this.props.work.dateCreated} </span>
            <span> Deadline: {this.props.work.dateEnd} </span> */}
            <div>
              <span> Status: {this.renderStatus(this.props.work.status)} </span>
            </div>
            <div>
              <span> Date Created: {this.props.work.dateCreated} </span>
            </div>
            <div>
              <span> Date End: {this.props.work.dateEnd} </span>
            </div>
            <button type="button" className="btn btn-default align-self-right" data-toggle="modal" data-target={id}>
              <span aria-hidden="true">Edit</span>
            </button>
            <div className="modal fade" id={this.props.work.id} tabIndex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="false">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Add New Work</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={this.submit}>
                      <div className="form-group row">
                        <label className="col-3">Title</label>
                        <input className="col-8 form-control"
                          type="text"
                          ref="title"
                          defaultValue={this.props.work.title}
                          required />
                      </div>
                      <div className="form-group row">
                        <label className="col-3">Description</label>
                        <textarea className="col-8 form-control"
                          rows={5} ref="description"
                          defaultValue={this.props.work.description}
                          required ></textarea>
                      </div>
                      <div className="form-group row">
                        <label className="col-3">Status</label>
                        <select className="col-8 form-control"
                          ref="status"
                          defaultValue={status}
                          required >
                          <option value="0">To do</option>
                          <option value="1">Doing</option>
                          <option value="2">Done</option>
                          <option value="3">Pending</option>
                        </select>
                      </div>
                      <div className="form-group row">
                        <label className="col-3">Date end</label>
                        <input className="col-8 form-control"
                          type="date"
                          ref="dateEnd"
                          defaultValue={this.props.work.dateEnd}
                          required />
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" className="btn btn-primary">Save changes</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
