import * as React from 'react';
import { Layout } from './layout/layout';
import { AddNewModal } from './addNewModal';
import { WorkCardList } from './../../components/workCardList';

export class MainPage extends React.Component {

  constructor(props) {
    super(props)
    let today = new Date();
    this.state = {
      workList: [{
        title: "Study Japanese",
        description: "Learn Vocabularies Lesson 1",
        status: "0",
        dateEnd: "2017-12-20",
        dateCreated: today.getFullYear()+"-"+today.getMonth()+"-"+today.getDate()
      }]
    }

    this.addNewWork = this.addNewWork.bind(this);
    this.editWork = this.editWork.bind(this);
    this.deleteWork = this.deleteWork.bind(this);
  }

  addNewWork(newWork) {
    this.setState({ workList: [...this.state.workList, newWork] })
  }

  editWork(position, work) {
    var workListEdited = this.state.workList;
    workListEdited[position] = work;
    this.setState({ workList: workListEdited });
  }

  deleteWork(position) {
    var workListDeleted = this.state.workList;
    workListDeleted.splice(position, 1);
    this.setState({workList:workListDeleted})
  }

  render() {
    return (
      <Layout>
        <AddNewModal onAddNewWork={this.addNewWork} />
        <WorkCardList workList={this.state.workList} 
                      editWork={this.editWork} 
                      deleteWork={this.deleteWork} />
      </Layout>
    )
  }
}
