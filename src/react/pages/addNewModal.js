import * as React from 'react';

export class AddNewModal extends React.Component {

  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this);
  }

  submit(e) {
    e.preventDefault();
    let today = new Date();
    var work = {
      title: this.refs.title.value,
      description: this.refs.description.value,
      status: this.refs.status.value,
      dateEnd: this.refs.dateEnd.value,
      dateCreated: today.getFullYear()+"-"+today.getMonth()+"-"+today.getDate()
    }

    this.props.onAddNewWork(work);

    this.refs.title.value = "";
    this.refs.description.value = "";
    this.refs.dateEnd.value = "";
    this.refs.status.value = 0;
    $(`#addNewModal`).modal('hide');
  }

  render() {

    return (
      <div>
        <button type="button" data-toggle="modal" data-target="#addNewModal" className="floatActionButton">
          +
        </button>

        <div className="modal fade" id="addNewModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Add New Work</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={this.submit}>
                  <div className="form-group row">
                    <label className="col-3">Title</label>
                    <input className="col-8 form-control"
                           type="text"
                           ref="title"
                           required />
                  </div>
                  <div className="form-group row">
                    <label className="col-3">Description</label>
                    <textarea className="col-8 form-control" rows={5} ref="description" required ></textarea>
                  </div>
                  <div className="form-group row">
                    <label className="col-3">Status</label>
                    <select className="col-8 form-control" ref="status" required >
                      <option value="0">To do</option>
                      <option value="1">Doing</option>
                      <option value="2">Done</option>
                      <option value="3">Pending</option>
                    </select>
                  </div>
                  <div className="form-group row">
                    <label className="col-3">Date end</label>
                    <input className="col-8 form-control" type="date" ref="dateEnd" required />
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" className="btn btn-primary">Save changes</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
