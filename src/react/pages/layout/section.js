import * as React from 'react';

export class Section extends React.Component {
  render() {
    return(
      <div className="col-9">
        <div className="row h-100">
          <div className="col-12" style={{position: "relative"}}>
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}
