import * as React from 'react';
import { Link } from 'react-router-dom';

export class Sidebar extends React.Component {
  render() {
    return(
      <div className="col-3 p-0" style={{background: "#00796B", textAlign: "center", color:"#ffffff"}}>
        <h1>DO OR DO NOT</h1>
        <span>{`THERE'S NO TRY`}</span>
        <div className="m-5"></div>
        <ul className="list-group">
          <Link className="list-group-item list-group-item-action active" to="/">List Of Work</Link>
          <Link className="list-group-item list-group-item-action" to="/about">About Me</Link>
        </ul>
      </div>
    )
  }
}
