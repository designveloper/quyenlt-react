import * as React from 'react';

import { Sidebar } from './sidebar';
import { Section } from './section';

export class Layout extends React.Component {

  render() {

    return(
      <div className="w-100" style={{overflow: "hidden", height: window.innerHeight+"px"}}>
        <div className="row w-100 h-100">
          <Sidebar />
          <Section>
            {this.props.children}
          </Section>
        </div>
      </div>
    )
  }
}
