import * as React from 'react';
import * as ReactDOM from "react-dom";
import { BrowserRouter,
    Route,
    Switch,
    withRouter } from 'react-router-dom';

//import { Router, Route, IndexRoute } from 'react-router';

//import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { MainPage } from './pages/mainPage';
import { About } from './pages/about';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={MainPage} />
      <Route exact path="/about" component={About} />
    </Switch>
  </BrowserRouter>
  ,
  document.getElementById("root")
)
