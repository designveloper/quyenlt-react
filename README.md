# SUMMARY #

### 1. Overview ###

Basic knowledge about ReactJS:
* React Component

* Router

* Props and State

* Babel and JSX

### 2. Detail ###

* Write config file for Webpack

* Creating components using React.createClass and ES6 syntax. Stateless Components

* Working with state (Variables of Class) and props (Variables from parents)

* Using Router, Route to navigating the page (Have to use Link tag to make it working)

* Function binding

* Understanding about component lifecycle

### 3. Conclusion ###

Have basic knowledge but I meet a lot of problems when making a project because different version and a lot of thing have been outdated. As a result I have find new way to make it run.