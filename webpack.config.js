var webpack = require('webpack');

module.exports = {
  entry: './src/react/script.js',
  output: {
    path: __dirname + '/src/statics/js',
    filename: 'bundle.js'
  },
  devServer: {
    inline: true,
    contentBase: './src/statics/',
    port: 4000,
    publicPath: '/',
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        query: {
          presets: ['env', 'react']
        }
      }
    ]
  }
};
